<?php
/**
 * @file
 * commerce_discount_coupon_exports.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function commerce_discount_coupons_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'commerce_discount_coupons_review_pane';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'commerce_order';
  $view->human_name = 'Commerce Discount Coupons Review Pane';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Checkout';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['discount_coupons_code_reference_target_id']['id'] = 'discount_coupons_code_reference_target_id';
  $handler->display->display_options['relationships']['discount_coupons_code_reference_target_id']['table'] = 'field_data_discount_coupons_code_reference';
  $handler->display->display_options['relationships']['discount_coupons_code_reference_target_id']['field'] = 'discount_coupons_code_reference_target_id';
  $handler->display->display_options['relationships']['discount_coupons_code_reference_target_id']['required'] = TRUE;
  /* Field: Commerce Discount: Internal, numeric commerce discount ID */
  $handler->display->display_options['fields']['discount_id']['id'] = 'discount_id';
  $handler->display->display_options['fields']['discount_id']['table'] = 'commerce_discount';
  $handler->display->display_options['fields']['discount_id']['field'] = 'discount_id';
  $handler->display->display_options['fields']['discount_id']['relationship'] = 'discount_coupons_code_reference_target_id';
  $handler->display->display_options['fields']['discount_id']['label'] = '';
  $handler->display->display_options['fields']['discount_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['discount_id']['element_label_colon'] = FALSE;
  /* Field: Commerce Discount: Coupon Code */
  $handler->display->display_options['fields']['commerce_discount_coupons_code']['id'] = 'commerce_discount_coupons_code';
  $handler->display->display_options['fields']['commerce_discount_coupons_code']['table'] = 'field_data_commerce_discount_coupons_code';
  $handler->display->display_options['fields']['commerce_discount_coupons_code']['field'] = 'commerce_discount_coupons_code';
  $handler->display->display_options['fields']['commerce_discount_coupons_code']['relationship'] = 'discount_coupons_code_reference_target_id';
  /* Field: Commerce Discount: Label */
  $handler->display->display_options['fields']['label']['id'] = 'label';
  $handler->display->display_options['fields']['label']['table'] = 'commerce_discount';
  $handler->display->display_options['fields']['label']['field'] = 'label';
  $handler->display->display_options['fields']['label']['relationship'] = 'discount_coupons_code_reference_target_id';
  $handler->display->display_options['fields']['label']['label'] = '';
  $handler->display->display_options['fields']['label']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Remove';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $translatables['commerce_discount_coupons_review_pane'] = array(
    t('Master'),
    t('Checkout'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Commerce Discount entity referenced from discount_coupons_code_reference'),
    t('.'),
    t(','),
    t('Coupon Code'),
    t('Remove'),
    t('Block'),
  );
  $export['commerce_discount_coupons_review_pane'] = $view;

  return $export;
}
