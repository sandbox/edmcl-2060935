<?php
/**
 * @file
 *
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_discount_coupons_rules_condition_info() {
  $conditions = array();
  if (module_exists('commerce_discount')) {
    $conditions['commerce_discount_coupons_coupon_is_applied_to_order'] = array(
    'label' => t('Order has coupon applied.'),
    'group' => t('Commerce Discount Coupon'),
    'parameter' => array(
      'entity' => array(
        'label' => t('Entity'),
        'type' => 'entity',
        'wrapped' => TRUE,
      ),
      'discount' => array(
        'label' => t('Commerce discount'),
        'type' => 'entity',
        'wrapped' => TRUE,
      ),
     ),
    'callbacks' => array(
      'execute' => 'commerce_discount_coupons_coupon_is_applied_to_order',
     ),
   );
  }
  return $conditions;
}

/**
 * Implements hook_rules_event_info().
 *
 */
function commerce_discount_coupons_rules_event_info() {
  $events = array();
  $events['commerce_discount_coupons_validate'] = array(
    'label' => t('Validate a Commerce Discount Coupon'),
    'group' => t('Commerce Discount Coupons'),
    'variables' => array(
      'coupon' => array(
        'type' => 'commerce_discount_coupons_coupon',
        'label' => t('commerce discount coupon to validate')
      ),
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );
  $events['commerce_discount_coupons_redeem'] = array(
    'label' => t('Redeem a Commerce Discount Coupon'),
    'group' => t('Commerce Discount Coupons'),
    'variables' => array(
      'coupon' => array(
        'type' => 'commerce_discount',
        'label' => t('Commerce discount coupon to redeem')
      ),
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Commerce Order'),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );
  return $events;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_discount_coupons_rules_action_info() {
  $actions = array();
  $actions['commerce_discount_coupons_code_is_valid'] = array(
    'label' => t('Set coupon as valid'),
    'parameter' => array(
      'override' => array(
        'type' => 'boolean',
        'label' => t('Override'),
        'description' => t('Normally, the validation result is concatenated by an AND operator. Here you can bypass this and set the value to true.'),
      ),
    ),
    'group' => t('Commerce Discount Coupon'),
    'base' => 'commerce_discount_coupons_code_is_valid',
    'callbacks' => array(
      'execute' => 'commerce_discount_coupons_code_is_valid',
    ),
  );
  $actions['commerce_discount_coupons_action_add_coupon_code_to_order'] = array(
    'label' => t('Apply coupon to order'),
    'parameter' => array(
      'coupon' => array(
        'type' => 'commerce_discount',
        'label' => t('Commerce Discount Coupon'),
      ),
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Commerce Order'),
      ),
    ),
  'group' => t('Commerce Discount Coupon'),
    'base' => 'commerce_discount_coupons_action_add_coupon_code_to_order',
    'callbacks' => array(
      'execute' => 'commerce_discount_coupons_action_add_coupon_code_to_order',
    ),
  );
  $actions['commerce_discount_coupons_action_get_coupons_for_order'] = array(
    'label' => t('Get coupons for order'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Commerce order'),
      ),
    ),
    'provides' => array(
      'order_coupons' => array(
        'type' => 'list<commerce_discount>',
        'label' => t('Coupons attached to this order'),
      ),
    ),
    'group' => t('Commerce Discount Coupon'),
    'base' => 'commerce_discount_coupons_action_get_coupons_for_order',
  );
  $actions['commerce_discount_coupons_action_remove_coupon_from_order'] = array(
    'label' => t('Remove a coupon from an order'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Commerce Order'),
      ),
      'commerce_discount_coupons_coupon' => array(
        'type' => 'commerce_discount_coupons_coupon',
        'label' => t('Commerce Discount Coupon'),
      ),
    ),
    'group' => t('Commerce Discount Coupon'),
  );
  $actions['commerce_discount_coupons_action_remove_all_coupons_from_order'] = array(
    'label' => t('Remove all coupons from an order'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Commerce Order'),
      ),
    ),
    'group' => t('Commerce Discount Coupon'),
  );
  return $actions;
}

/**
 * Apply a Discount Coupon to an Order
 *
 * @param $coupon
 * @param $commerce_order
 *
 */
function commerce_discount_coupons_action_add_coupon_code_to_order($coupon, $order) {
  if (!($order instanceof EntityMetadataWrapper)) {
    $order = entity_metadata_wrapper('commerce_order', $order);
  }
  if (!($coupon instanceof EntityMetadataWrapper)) {
    $coupon = entity_metadata_wrapper('commerce_discount', $coupon);
  }

  $coupons = array();

  foreach($order->discount_coupons_code_reference as $order_coupon) {
    $coupons[] = $order_coupon->discount_id->value();
  }

  $coupons[] = $coupon->discount_id->value();

  $order->discount_coupons_code_reference->set($coupons);
}

/**
 * Action to remove a coupon from a given order.
 */
function commerce_discount_coupons_action_remove_coupon_from_order($order, $coupon) {
  if (empty($coupon) || empty($order)) {
    return array();
  }
  commerce_discount_coupons_remove_coupon_from_order($order, $coupon);
}

/**
 * Action to remove all coupons from a given order.
 */
function commerce_discount_coupons_action_remove_all_coupons_from_order($order) {
  if (empty($order)) {
    return array();
  }
  commerce_discount_coupons_remove_all_coupons_from_order($order);
}

/**
 * Condition to determine if a discount coupon has been applied to an order
 *
 * @param $line_item
 *  Commerce Order line item
 * @return boolean
 */
function commerce_discount_coupons_coupon_is_applied_to_order($line_item, $discount) {
  $order_id = $line_item->order_id->value();
  $order = entity_load_single('commerce_order',$order_id);
  $order_wrapper = entity_metadata_wrapper('commerce_order',$order);
  $coupon_code = $discount->commerce_discount_coupons_code->value();

  foreach ($order_wrapper->discount_coupons_code_reference as $order_coupon) {
    $order_coupon_code = $order_coupon->commerce_discount_coupons_code->value();
    if ($order_coupon_code == $coupon_code) {
      return true;
    }
  }
  return false;
}