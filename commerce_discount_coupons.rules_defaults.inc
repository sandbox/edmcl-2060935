<?php
/**
 * @file
 * commerce_discount_coupon_exports.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_discount_coupons_default_rules_configuration() {
  $items = array();
  $items['rules_redeem_a_discount_coupon'] = entity_import('rules_config', '{ "rules_redeem_a_discount_coupon" : {
      "LABEL" : "Redeem a Discount Coupon",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "commerce discount coupons" ],
      "REQUIRES" : [ "commerce_discount_coupons" ],
      "ON" : [ "commerce_discount_coupons_redeem" ],
      "DO" : [ { "commerce_discount_coupons_action_add_coupon_code_to_order" : [] } ]
    }
  }');
  return $items;
}
